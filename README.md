# slack

A Crystal shard for quickly creating Slack applications.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  slack:
    github: msts/slack
```

## Example Usage

```crystal
require "slack"

Slack.command "help" do
  <<-END
  I understand:
    *help* - This help text
    *say {something}* - Repeat what you say
    *say hi to {user}* - Say hi to the nominated user

  To get help about a command use `{command} help`.
  END
end

Slack.command "say help" do |ctx, vars|
  <<-END
  Type `say something` and I'll say `something`, or try `say "this long string"` and I'll say that!
  END
end

Slack.command "say {something}" do |ctx, vars|
  "#{vars["something"].upcase}!"
end

Slack.variable "user", /\<@.+\>/ do |result|
  result
end

Slack.command "say hi to {user}" do |ctx, vars|
  "Hello #{vars["user"]} from <@#{ctx["user_id"]}>!"
end

Slack.command "say {something} to {user}" do |ctx, vars|
  "#{vars["something"]} #{vars["user"]} from <@#{ctx["user_id"]}>!"
end

Slack.run
```


## Contributing

1. Fork it (<https://github.com/msts/slack/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [sbaldwin](https://github.com/sbaldwin) Steve Baldwin - creator, maintainer
- [skaragianis](https://github.com/skaragianis) Steffan Karagianis - layabout
