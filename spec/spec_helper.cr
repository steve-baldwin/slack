require "spec"

def create_args(str : String) : Hash(String, String)
  {
    # "token"        => "...",
    # "team_id"      => "...",
    # "team_domain"  => "...",
    # "channel_id"   => "...",
    # "channel_name" => "...",
    # "user_id"      => "...",
    # "user_name"    => "...",
    # "command"      => "/abc",
    "text" => str,
    # "response_url" => "..."
    # "trigger_id"   => "...",
  }
end
