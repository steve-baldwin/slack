# :nodoc:
class HTTP::Server
  class Context
    @request_id = 0
    @t_start = Time.now
    @logger : Logger?
    @endpoint = ""
    @params = HTTP::Params.parse("")

    def start_request(logger : Logger)
      @t_start = Time.now
      @logger = logger
      @request_id = Random.new.rand(100000)
      logger.warn("#{@request_id}|#{self.request.method} #{self.request.resource} start")
    end

    def log_info(msg : String)
      if l = @logger
        l.info("#{@request_id}|#{msg}")
      end
    end

    def log_warn(msg : String)
      if l = @logger
        l.warn("#{@request_id}|#{msg}")
      end
    end

    def log_error(msg : String)
      if l = @logger
        l.error("#{@request_id}|#{msg}")
      end
    end

    def end_request
      ms = (Time.now - @t_start).total_milliseconds.round(3)
      log_warn("#{self.request.method} #{self.request.resource} completed (#{self.response.status_code}) in #{ms}ms")
    end

    def endpoint=(val : String)
      @endpoint = val
    end

    def endpoint
      @endpoint
    end

    def params=(body : String)
      @params = HTTP::Params.parse(body)
    end

    def params
      @params
    end
  end
end
