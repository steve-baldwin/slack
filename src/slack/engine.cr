require "string_scanner"

# module Slack::Command
#   abstract def call(args : Hash(String, String), vars : Hash(String, String)) : String

#   alias Block = (Hash(String, String), Hash(String, String)) -> (String)
# end

module Slack
  private class Command
    getter tokens : Array(Lexer::Token)
    getter command : CommandProcessor
    getter async : Bool
    getter async_message : String?

    def initialize(@tokens, @command, @async, @async_message)
    end
  end

  private class Lexer
    RSTRING   = /("|'|‘|“)[^("|'|’|”)]*("|'|’|”)/
    RVARIABLE = /\{[^\}]*}/

    enum Type
      Constant
      String
      Variable
    end

    alias Token = NamedTuple(value: String, var: Variable, type: Type)
    private alias Variable = NamedTuple(regex: Regex, after: ((String) -> (String | Nil)) | Nil)

    def self.lex(str : String, variables : Hash(String, Variable))
      result = Array(Token).new
      s = StringScanner.new(str)

      until s.eos?
        if s.check(/\w/) # Is this a constant?
          # Grab the constant
          r = s.scan(/\w*/).not_nil!
          result << {value: r, var: {regex: Regex.new(r.not_nil!), after: nil}, type: Type::Constant}
        elsif s.check(/\"/) # Is this the beginning of a string?
          r = s.scan(RSTRING)
          raise UnboundedStringException.new("Unbounded string at #{s.offset + 1} in \"#{str}\"") if r == nil
          r = r.not_nil!
          r = r.lchop.rchop.lchop.rchop
          result << {value: r, var: {regex: RSTRING, after: nil}, type: Type::String}
        elsif s.check(/\{/) # Is this the beginning of a variable?
          r = s.scan(RVARIABLE)
          raise UnboundedVariableException.new("Unbounded variable declaration at #{s.offset + 1} in \"#{str}\"") if r == nil
          r = r.not_nil!
          r = r.lchop('{').rchop('}')
          variable = variables[r]? || {regex: /[^\s]*/, after: nil}
          result << {value: r, var: variable, type: Type::Variable}
        elsif s.check(/\s*/)
          s.scan(/\s*/)
        else
          raise UnrecognizedSequenceException.new("Unrecognised sequence at #{s.offset + 1} in \"#{str}\"")
        end

        # Gobble whitespace or hit the end of the string
        s.scan(/\s*/)
      end

      result
    end
  end

  private class Engine
    private alias Variable = NamedTuple(regex: Regex, after: ((String) -> (String | Nil)) | Nil)

    def initialize(@commands = Array(Command).new, @variables = Hash(String, Variable).new)
    end

    def add_command(grammar : String, command : CommandProcessor, async = false, async_message : String? = nil)
      tokens = Lexer.lex(grammar, @variables)

      @commands << Command.new(tokens, command, async, async_message)
    end

    def add_variable(name : String, regex : Regex, block : (String) -> (String | Nil))
      @variables[name] = {regex: regex, after: block}
    end

    def parse(input : String, commands : Array(Command)) : NamedTuple(command: Command, vars: Hash(String, String) | Nil) | Nil
      matches = Array(NamedTuple(command: Command, vars: Hash(String, String))).new
      commands.each do |command|
        vars = Hash(String, String).new
        remainder = input
        pos = 0
        command.tokens.each do |token|
          remainder = remainder.strip
          if value = token[:var][:regex].match remainder
            if token[:type] == Lexer::Type::Variable
              vars[token[:value]] = value[0]
            elsif token[:type] == Lexer::Type::String
              vars[token[:value]] = value[0].lchop.rchop
            end
            pos += 1
            remainder = value.post_match
            break if remainder.blank?
          end
        end
        matches << {command: command, vars: vars} if pos == command.tokens.size
      end

      # Return the longest matching command
      return matches.max_by { |a| a[:command].tokens.size } if matches.size > 0
    end

    def execute(args : Hash(String, String)) : String
      result = parse(args["text"], @commands)
      if result
        result[:command].command.call(args, result[:vars])
      else
        Slack.config.dont_know
      end
    end
  end
end
