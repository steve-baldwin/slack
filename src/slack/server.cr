require "http/server"
require "logger"
require "openssl/**"
require "./ext/context"
require "./ext/params"

module Slack
  private SERVER_PORT = ENV["SERVER_PORT"]? || "3000"
  private ENDPOINT_RX = /^\/slack\/(?<endpoint>cmd|action)\/?$/

  # Raised when a request from Slack is malformed
  private class BadRequestException < Exception
  end

  # Raised when the Slack request cannot be validated as being from Slack
  private class SlackSigningException < Exception
  end

  private class LogHandler
    include HTTP::Handler

    def initialize(@logger : Logger)
    end

    def call(context)
      context.start_request(@logger)
      begin
        call_next(context)
        context.end_request
      rescue e : Exception
        context.log_error(e.inspect_with_backtrace)
        raise e
      end
      context
    end
  end

  private class SlackHandler
    include HTTP::Handler

    SLACK_SIG_VERSION    = "v0"
    SLACK_SIGNING_SECRET = ENV["SLACK_SIGNING_SECRET"]
    MAX_CLOCK_DRIFT_SEC  = 60

    @runner : Hash(String, String) -> (String)

    def initialize(@runner)
    end

    def call(context)
      r = context.request
      #
      # Bail if not POST
      #
      unless r.method == "POST"
        raise BadRequestException.new
      end
      #
      # Bail unless /slack/{cmd|action}
      #
      m = ENDPOINT_RX.match(r.resource)
      raise BadRequestException.new unless m
      #
      # Slurp in the body
      #
      io = context.request.body.not_nil!
      body = io.gets_to_end
      #
      # Unless disabled, check the request is signed by Slack
      #
      unless ENV["NO_AUTH_SLACK"]?
        ts = r.headers["X-Slack-Request-Timestamp"]
        now_e = Time.utc_now.epoch
        if (now_e - ts.to_i).abs > MAX_CLOCK_DRIFT_SEC
          raise SlackSigningException.new("Clock drift exceeded (#{now_e} - #{ts})")
        end
        header_sig = r.headers["X-Slack-Signature"]
        base_string = SLACK_SIG_VERSION + ":" + ts + ":" + body
        calc_sig = SLACK_SIG_VERSION + "=" + OpenSSL::HMAC.hexdigest(:sha256, SLACK_SIGNING_SECRET, base_string)
        if header_sig != calc_sig
          raise SlackSigningException.new("Invalid Slack signature")
        end
      end
      #
      # Parse the body
      #
      context.params = body
      #
      # Process the Slack command or action
      #
      context.response.print @runner.call(context.params.fetch)

      return call_next(context)
    rescue e : BadRequestException
      context.response.status_code = 400
    rescue e : SlackSigningException
      context.log_error(e.message || "Oops")
      context.response.status_code = 400
    end
  end

  private class Server
    @logger : Logger
    @server : HTTP::Server

    def initialize(runner, @logger)
      handlers : Array(HTTP::Handler) = [
        HTTP::Handler.cast(HTTP::ErrorHandler.new),
        HTTP::Handler.cast(LogHandler.new(@logger)),
        HTTP::Handler.cast(SlackHandler.new(runner)),
      ]
      @server = HTTP::Server.new(handlers)
      @server.bind_tcp("0.0.0.0", SERVER_PORT.to_i)
    end

    def listen
      @logger.warn("Starting on port #{SERVER_PORT}")
      self.setup_trap_signal
      @server.listen
    end

    private def setup_trap_signal
      Signal::INT.trap do
        @logger.warn "Shutting down gracefully"
        @server.close unless @server.closed?
        exit
      end
    end
  end
end
