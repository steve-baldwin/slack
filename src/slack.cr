require "logger"
require "./slack/engine"
require "./slack/server"

# A Crystal shard for quickly creating Slack applications. See `.env.example` in the simple example application for needed environment variables.
module Slack
  extend self

  @@config = Config.new
  @@engine = Engine.new

  # The block or method that responds to a Slack request. It receives a hash of Slack provided context, a hash of parsed variables and must return a string response.
  alias CommandProcessor = (Hash(String, String), Hash(String, String)) -> (String)

  # Raised when a new `Slack#command grammar` contains an unbounded string
  class UnboundedStringException < Exception
  end

  # Raised when a new `Slack#command grammar` contains an unbounded variable declaration
  class UnboundedVariableException < Exception
  end

  # Raised when a new `Slack#command grammar` contains an unrecognized sequence of characters
  class UnrecognizedSequenceException < Exception
  end

  # The configuration object for Slack.
  class Config
    # The logger to use. It defaults to Logger.new(STDOUT).
    property logger : Logger = Logger.new(STDOUT)
    # The message returned to users when a command isn't recognised. It defaults to "I'm not sure how to respond to that?".
    property dont_know : String = "I'm not sure how to respond to that?"
  end

  # Present the `Config` object to your block so that you can override the defaults.
  def config(&block)
    yield @@config
  end

  # Add a new type of variable matcher that can be used by your commands. If matched, it will be provided to your `CommandProcessor` in the Variables hash.
  def variable(name : String, regex : Regex, &block : (String) -> (String | Nil))
    @@engine.add_variable(name, regex, block)
  end

  # Add a new type of command that your bot will respond to. This accepts a method of type `CommandProcessor`. The command grammar is of the form `constant1 constant2 {variable} "{string enclosed variable}"`.
  def command(grammar : String, command : CommandProcessor, async = false, async_message : String? = nil)
    @@engine.add_command(grammar, command, async, async_message)
  end

  # Add a new type of command that your bot will respond to. This accepts a block of type `Command Processor`. The command grammar is of the form `constant1 constant2 {variable} "{string enclosed variable}"`.
  def command(grammar : String, async = false, async_message : String? = nil, &command : CommandProcessor)
    self.command(grammar, command, async, async_message)
  end

  # Start the server.
  def run
    server = Server.new(->execute(Hash(String, String)), @@config.logger)
    server.listen
  end

  protected def config
    @@config
  end

  protected def execute(args : Hash(String, String)) : String
    @@engine.execute(args)
  end

  protected def reset
    @@config = Config.new
    @@engine = Engine.new
  end
end
