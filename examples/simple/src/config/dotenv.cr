module Dotenv
  extend self

  private DotEnvFile = ".env"

  def load(*, display = false)
    if File.exists? DotEnvFile
      File.read_lines(DotEnvFile).each do |line|
        next if line.starts_with? '#'
        key, value = line.strip.split "=", 2
        ENV[key.strip] = value.strip
      end
    else
      puts "WARN: #{DotEnvFile} does not exist"
    end
    pp ENV if display
  end
end
