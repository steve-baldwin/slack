require "slack"

class TestCommand
  include Slack::Command

  def call(args : Hash(String, String), vars : Hash(String, String))
    "Hello #{vars["user"]}!"
  end
end
