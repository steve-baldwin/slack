# simple slack-cr usage example

## Installation

`shards install`

## Usage

1. Visit https://api.slack.com/
2. Create a new application
3. Copy .env.example to .env, or use environment variables
4. Update SLACK_CLIENT_SECRET with your Client Secret
5. Update SLACK_SIGNING_SECRET with your Signing secret
6. `crystal run ./scr/simple.cr